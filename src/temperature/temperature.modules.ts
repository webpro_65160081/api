import { Module } from '@nestjs/common';
import { TemperatureController } from './temperature.controller';
import { TemperatureService } from './temperature.service';

@Module({
  imports: [],
  providers: [TemperatureService],
  controllers: [TemperatureController],
  exports: [],
})
export class TemperatureModule {}
